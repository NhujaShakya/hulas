//
//  SMLoginManager.swift
//  CNKhana
//
//  Created by Santosh Maharjan on 9/18/20.
//  Copyright © 2020 Cyclone Nepal Info Tech. All rights reserved.
//

import Foundation


class SMLoginManager {
    
    static func isLoggedIn() -> Bool {
        let token = SMUserDefault.get(forKey: .token) as? String ?? ""
        return !token.isEmpty
    }
    
    static func clearLogSessions() {
        SMUserDefault.clearAsNeeded()
    }
    
}
