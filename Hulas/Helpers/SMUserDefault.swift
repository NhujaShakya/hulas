//
//  SMUserDefault.swift
//  CNRestaurant
//
//  Created by Santosh Maharjan on 8/30/20.
//  Copyright © 2020 Nhuja Shakya. All rights reserved.
//

import Foundation

import Foundation

class SMUserDefault {
    
    enum defaultType:String {
        // String
        case token           = "com.cninfotech.token"
        case firstName       = "com.cninfotech.firstName"
        case lastName        = "com.cninfotech.lastName"
    }
    
    class func set(data:Any, forKey :defaultType) {
        UserDefaults().setValue(data, forKey: forKey.rawValue)
        UserDefaults().synchronize()
    }
    
    class func get(forKey key:defaultType) -> Any? {
        return UserDefaults().object(forKey: key.rawValue)
    }
    
    class func isAvailable(forKey key:defaultType) -> Bool {
        guard let _ = UserDefaults().object(forKey: key.rawValue) else { return false }
        return true
    }
    
    class func clear(_ type:defaultType? = nil) {
        if let _type = type {
            /*   Clean specific NSUserDefault data  */
            UserDefaults().removeObject(forKey: _type.rawValue)
        }else{
            /*   Clean all NSUserDefault datas   */
            guard let appDomain = Bundle.main.bundleIdentifier else { return }
            UserDefaults().removePersistentDomain(forName: appDomain)
        }
    }
    
    class func clearAsNeeded() {
        SMUserDefault.clear(.token)
        SMUserDefault.clear(.firstName)
        SMUserDefault.clear(.lastName)
    }
    
}
