//
//  SignUpApi.swift
//  Hulas
//
//  Created by Nhuja on 11/11/20.
//

import UIKit
import SwiftyJSON
import Alamofire

class SignUpApi {
    
    func getSignUp(user: User, completion: @escaping CompletionBlockSignUp) {
        let url = URL(string: SIGNUP_URL)!
        var userDistrict = ""
        
        if user.district.id != 0 {
            userDistrict = "\(user.district.id)"
        }
        let paramToSend = [
            "first_name"    :      user.firstName,
            "last_name"     :       user.lastName,
            "mobile_number" :   user.mobile,
            "province_id"   :     user.province.id,
            "districts_id"  :    userDistrict,
            "metropolitan"  :    user.metropolitan,
            "ward_no"       :         user.wardNo,
            "street_address":  user.street,
            "pan_vat"       :         user.vatPan,
            "is_vat_pan"    :      user.vatPanBool,
            "latitude"      :        23.67,
            "longitude"     :       32.33,
            "image"         :           user.image,
            "email"         :           user.email,
            "username"      :        user.userName,
            "password"      :        user.password,
            "password_confirmation" : user.password
        ] as [String: Any]
        
        
        AF.request(url,method: .post, parameters: paramToSend, encoding: URLEncoding.default){ $0.timeoutInterval = 60 }.response {
            response in
            if let error = response.error {
                DispatchQueue.main.async {
                    completion(false, error.localizedDescription)
                }
            } else {
                switch response.result {
                case .success(let data):
                    if let _data = data {
                        let json = JSON(_data)
                        let status = json["status"].boolValue
                        let status_code = json["status_code"].intValue
                        var message = json["message"].stringValue
                        let data = json["data"].dictionaryValue
                        
                        if status_code == 400 {
                            SMLoginManager.clearLogSessions()
                            let error = data["error"]!.dictionaryValue
                            let username = error["username"]?.stringValue ?? ""
                            let pan_vat = error["pan_vat"]?.string ?? ""
                            let email = error["email"]?.string ?? ""
                            let mobile_number = error["mobile_number"]?.stringValue ?? ""
                            
//                            guard message == "invalid_validation" else {
//                                return
//                            }
                            message = ""
                            var existingMsg:String = ""
                            
                            if !username.isEmpty {
                                message = " UserName: \(username)"
                            }
                            if !pan_vat.isEmpty {
                                existingMsg = message
                                if message.isEmpty {
                                    message = " Pan/Vat: \(pan_vat)"
                                } else {
                                    message = "\(existingMsg)\n Pan/Vat: \(pan_vat)"
                                }
                                
                            }
                            if !email.isEmpty {
                                existingMsg = message
                                if message.isEmpty {
                                    message = "Email:\(email)"
                                } else {
                                    message = "\(message)\n Email:\(email)"
                                }
                            }
                            if !mobile_number.isEmpty {
                                existingMsg = message
                                if message.isEmpty {
                                    message = "Mobile:\(mobile_number)"
                                } else {
                                    message = "\(message)\n Mobile:\(mobile_number)"
                                }
                            }
                        }
                        
                        DispatchQueue.main.async {
                            if status {
                                let token = data["token"]!.stringValue
                                let firstName = data["first_name"]!.stringValue
                                let lastName = data["last_name"]!.stringValue
                                SMUserDefault.set(data: token, forKey: .token)
                                SMUserDefault.set(data: firstName, forKey: .firstName)
                                SMUserDefault.set(data: lastName, forKey: .lastName)
                            }
                            completion(status, message)
                        }
                    }
                    break
                    
                case .failure(let error):
                    DispatchQueue.main.async {
                        completion(false, error.localizedDescription)
                    }
                    break
                }
                
            }
        }
    }
}
