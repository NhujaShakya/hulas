//
//  ProvinceApi.swift
//  Hulas
//
//  Created by Nhuja on 11/11/20.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProvinceApi {
    var isLoading = false { didSet { print("isLoading changed to \(isLoading)") } }
    func getProvince(completion: @escaping (_ success: Bool, _ message: String, _ districts: [Province]) -> ()) {
        if isLoading { return }
        let url = URL(string: PROVINCE_URL)!
        isLoading = true
        
        
        AF.request(url,method: .get, encoding: JSONEncoding.default).response {
            response in
            self.isLoading = false
            if let error = response.error {
                DispatchQueue.main.async {
                    completion(false, error.localizedDescription, [])
                }
            } else {
                switch response.result {
                case .success(let data):
                    if let _data = data {
                        let json = JSON(_data)
                        let status = json["status"].boolValue
                        let message = json["message"].stringValue
                        
                        DispatchQueue.main.async {
                            
                            let provinces = Province.getProvinces(json: json)
                            completion(status, message, provinces)
                        }
                    }
                    break
                    
                case .failure(let error):
                    DispatchQueue.main.async {
                        completion(false, error.localizedDescription, [])
                    }
                    break
                }
                
            }
        }
    }
}
