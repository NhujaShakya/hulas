//
//  LoginApi.swift
//  Hulas
//
//  Created by Nhuja on 11/11/20.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class LoginApi {
    func getLogin(panVatMobile: String, password: String, completion: @escaping (_ success: Bool, _ message: String) -> () ) {
        let url = URL(string: LOGIN_URL)!
        let paramToSend = [
            "username" : panVatMobile,
            "password" : password
        ] as [String: Any]
        
        
        AF.request(url,method: .post, parameters: paramToSend, encoding: JSONEncoding.default).response {
            response in
            if let error = response.error {
                DispatchQueue.main.async {
                    completion(false, error.localizedDescription)
                }
            } else {
                switch response.result {
                case .success(let data):
                    if let _data = data {
                        let json = JSON(_data)
                        let status = json["status"].boolValue
                        let status_code = json["status_code"].intValue
                        var message = json["message"].stringValue
                        let data = json["data"].dictionaryValue
                        
                        if status_code == 400 {
                            SMLoginManager.clearLogSessions()
                            message = data["error"]!.stringValue
                        }
                        
                        DispatchQueue.main.async {
                            if status {
                                let token = data["token"]!.stringValue
                                let firstName = data["first_name"]!.stringValue
                                let lastName = data["last_name"]!.stringValue
                                SMUserDefault.set(data: token, forKey: .token)
                                SMUserDefault.set(data: firstName, forKey: .firstName)
                                SMUserDefault.set(data: lastName, forKey: .lastName)
                            }
                            completion(status, message)
                        }
                    }
                    break
                    
                case .failure(let error):
                    DispatchQueue.main.async {
                        completion(false, error.localizedDescription)
                    }
                    break
                }
                
            }
        }
    }
}
