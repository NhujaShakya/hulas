//
//  LogoutApi.swift
//  Hulas
//
//  Created by Nhuja on 11/11/20.
//

import UIKit
import Alamofire
import SwiftyJSON

class LogoutApi {
    
    func getLogoutApi(completion: @escaping CompletionBlock) {
        
        let url = URL(string: LOGOUT_URL)!
        let token = SMUserDefault.get(forKey: .token) as? String ?? ""
        let header = [
            "Authorization" : "Bearer \(token)",
            "Accept" : "application/json",
        ]
        
        AF.request(url, method: .get, encoding: URLEncoding.default, headers: HTTPHeaders.init(header)).response { response in
            
            if let error = response.error {
                debugPrint(error.localizedDescription)
                DispatchQueue.main.async {
                    completion(false,"\(error.localizedDescription)")
                }
            } else {
                switch response.result {
                case .success(let data):
                    if let _data = data {
                        let json = JSON(_data)
                        let status = json["status"].boolValue
                        let message = json["message"].stringValue
                        DispatchQueue.main.async {
                            completion(status,message)
                        }
                    }
                    break
                case .failure(let error):
                    print(error.errorDescription ?? "error from cnfood server")
                    break
                }
            }
        }
    }
}
