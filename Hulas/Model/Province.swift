//
//  Province.swift
//  Hulas
//
//  Created by Nhuja on 11/11/20.
//

import UIKit
import SwiftyJSON

class Province {
    var id: Int = 0
    var name: String = ""
    var districts: [District] = []
    
    
    static func getProvinces(json: JSON) -> [Province] {
        let data = json["data"].dictionaryValue
        let provinces = data["province"]!.arrayValue
        var allProvinces = [Province]()
        
        for province in provinces {
            let _province = Province()
            _province.id = province["id"].intValue
            _province.name = province["name"].stringValue
            _province.districts = District.getDistricts(districts: province["districts"].arrayValue)
            allProvinces.append(_province)
        }
        
        return allProvinces
    }
}
