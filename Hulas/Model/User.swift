//
//  User.swift
//  Hulas
//
//  Created by Nhuja on 11/11/20.
//

import UIKit

class User {
    var firstName: String = ""
    var lastName: String = ""
    var image: String = ""
    var mobile: String = ""
    var province: Province = Province()
    var district: District = District()
    var metropolitan: String = ""
    var wardNo : String = ""
    var street: String = ""
    var email: String = ""
    var userName: String = ""
    var password: String = ""
    var vatPan: String = ""
    var vatPanBool: Bool = false //1 is for Vat and 0 is for Pan
    
    var latitude: String = ""
    var longitude: String  = ""
    
}
