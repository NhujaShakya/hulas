//
//  District.swift
//  Hulas
//
//  Created by Nhuja on 11/10/20.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class District {
    
    var id :Int = 0
    var name: String = ""
    
    static func getDistricts(districts: [JSON]) -> [District] {
        var allDistricts = [District]()
        for district in districts {
            let _district = District()
            _district.id = district["id"].intValue
            _district.name = district["name"].stringValue
            allDistricts.append(_district)
        }
        return allDistricts
    }
}
