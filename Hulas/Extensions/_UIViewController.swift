//
//  _UIViewController.swift
//  PicturePerfect
//
//  Created by Nhuja Shakya on 5/1/20.
//  Copyright © 2020 Nhuja Shakya. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func simpleAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert,animated: true, completion: nil)
    }
    
    func showToast(message: String) {
        let toastLbl = UILabel(frame: CGRect(x: self.view.frame.width/2-75, y: self.view.frame.height/2 + 50, width: 150, height: 40))
        toastLbl.textAlignment = .center
        toastLbl.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        toastLbl.textColor = .white
        toastLbl.alpha = 1.0
        toastLbl.layer.cornerRadius = 20
        toastLbl.clipsToBounds = true
        toastLbl.text = message
        self.view.addSubview(toastLbl)
        UIView.animate(withDuration: 5.0, delay: 1.0,options: .curveEaseInOut, animations: {
            toastLbl.alpha = 0.0
        }) { (isCompleted) in
            toastLbl.removeFromSuperview()
        }
    }
}

extension UIViewController {
    func smShowAlertController(_ title:String, message:String, actions: [UIAlertAction], style: UIAlertController.Style = .alert) {
        let popup = UIAlertController(title: title, message: message, preferredStyle: style)
        actions.forEach{popup.addAction($0)}
        
        DispatchQueue.main.async(execute: { () -> Void in
            self.present(popup, animated: true, completion: { () -> Void in
            })
        })
    }
    
    func smShowSuccessAlertController(message:String, handler: ((UIAlertAction) -> Void)? = nil) {
        smShowAlertController("Success", message: message, actions: [
            UIAlertAction.init(title: "Ok", style: .default, handler: handler)
        ])
    }
    
    func smShowErrorAlertController(message:String, handler: ((UIAlertAction) -> Void)? = nil) {
        smShowAlertController("Failure", message: message, actions: [
            UIAlertAction.init(title: "Ok", style: .default, handler: handler)
        ])
    }
}
