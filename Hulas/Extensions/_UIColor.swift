//
//  _UIColor.swift
//  CNFood
//
//  Created by Nhuja on 8/27/20.
//  Copyright © 2020 Cyclone Nepal Info Tech. All rights reserved.
//

import UIKit


extension UIColor {
    
    static let universalPurple = UIColor.colorFromHex("#7D5DE4")
    static let universalDarkPurple = UIColor.colorFromHex("#182A4C")
    
    static func colorFromHex(_ hex : String) -> UIColor {
        var hexString = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if hexString.hasPrefix("#") {
            hexString.remove(at: hexString.startIndex)
        }
        
        if hexString.count != 6 {
            return UIColor.gray
        }
        
        var rgb : UInt32 = 0
        Scanner(string: hexString).scanHexInt32(&rgb)
        return UIColor.init(red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
                            green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
                            blue: CGFloat(rgb & 0x0000FF) / 255.0,
                            alpha: 1.0)
    }
}
