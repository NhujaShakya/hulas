//
//  _UIImage.swift
//  CNCard
//
//  Created by Nhuja Shakya on 1/31/20.
//  Copyright © 2020 Nhuja Shakya. All rights reserved.
//

import UIKit


extension UIImage {

    func isEqualsTo(_ image: UIImage?) -> Bool {
        if let _image = image {
         return self.pngData() == _image.pngData()
        }else{
            return false
        }
    }
}
