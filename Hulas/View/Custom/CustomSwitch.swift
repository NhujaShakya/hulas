//
//  CustomSwitch.swift
//  Hulas
//
//  Created by Nhuja on 11/10/20.
//

import UIKit

class CurveSwitch : UISwitch {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.onTintColor = AppColor
        self.tintColor = AppColor
        self.thumbTintColor = UIColor.white
        self.backgroundColor = AppColor
        self.layer.cornerRadius = 16
    }
}
