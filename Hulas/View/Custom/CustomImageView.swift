//
//  CustomImageView.swift
//  Hulas
//
//  Created by Nhuja on 11/10/20.
//

import UIKit

class CircleImageView: UIImageView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = layer.frame.size.width / 2
        clipsToBounds = true
    }
}
