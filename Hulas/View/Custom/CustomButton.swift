//
//  CustomButton.swift
//  Hulas
//
//  Created by Nhuja on 11/10/20.
//

import UIKit

class CurveButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
    }
}

class BorderButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderWidth = 1.0
        layer.cornerRadius = 5
        layer.borderColor = UIColor.colorFromHex("#D9D9D9").cgColor
        self.clipsToBounds = true
        contentHorizontalAlignment = .left
        contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
    }
}
