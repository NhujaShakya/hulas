//
//  LabelCell.swift
//  Hulas
//
//  Created by Nhuja on 11/10/20.
//

import UIKit

class LabelCell: UITableViewCell {
    
    static let identifier = "LabelCell"
    
    @IBOutlet weak var labelTxt: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }
    
    func configLabel(text: String) {
        labelTxt.text = text
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "LabelCell", bundle: nil)
    }
    
}
