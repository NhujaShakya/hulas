//
//  Constraints.swift
//  Hulas
//
//  Created by Nhuja on 11/10/20.
//

import UIKit


let AppColor = UIColor.colorFromHex("#4C5DB2")

let BASE_URL = "https://hulas.cninfotech.com"
let SM_URL = "\(BASE_URL)/api/1.0v"
let PROVINCE_URL = "\(SM_URL)/get-province-district"
let LOGIN_URL = "\(SM_URL)/login"
let SIGNUP_URL = "\(SM_URL)/register"
let LOGOUT_URL = "\(SM_URL)/logout"
let TERMSANDCONDITIONS_URL = "https://www.cninfotech.com/about/"

typealias CompletionBlockSignUp = (_ success: Bool, _ message: String) -> ()
typealias CompletionBlock = (_ success: Bool, _ message: String) -> ()
