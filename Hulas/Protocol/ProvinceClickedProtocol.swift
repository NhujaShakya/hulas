//
//  provinceClicked.swift
//  Hulas
//
//  Created by Nhuja on 11/10/20.
//

import UIKit

protocol ProvinceClickedProtocol: class {
    func getNewProvince(newProvince: String)
    func getNewDistrict(newDistrict: String)
}
