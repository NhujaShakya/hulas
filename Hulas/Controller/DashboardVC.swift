//
//  ViewController.swift
//  Hulas
//
//  Created by Nhuja on 11/10/20.
//

import UIKit
import SwiftSpinner

class DashboardVC: UIViewController {
    
    private var logoutApi = LogoutApi()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.title = "Dashboard"
        navigationController?.navigationBar.tintColor = AppColor
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(addTapped))
    }
    
    
    @objc func addTapped(_ sender: Any) {
        smShowAlertController("Alert", message: "Are you sure?", actions: [
            UIAlertAction(title: "OK", style: .destructive, handler: { (action) in
                SwiftSpinner.show("Logging out...")
                self.logoutApi.getLogoutApi { (status, message) in
                    SwiftSpinner.hide()
                    if status {
                        SMUserDefault.clearAsNeeded()
                        let naviController = UINavigationController()
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        naviController.pushViewController(vc, animated: true)
                        UIWindow.key?.rootViewController = naviController
                    } else {
                        if message == "Session Expired" {
                            self.smShowAlertController("Alert", message: "Session Expired", actions: [
                                UIAlertAction(title: "OK", style: .destructive, handler: { (action) in
                                    SMLoginManager.clearLogSessions()
                                    let naviController = UINavigationController()
                                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                    naviController.pushViewController(vc, animated: true)
                                    UIWindow.key?.rootViewController = naviController
                                })
                            ])
                        }
                        self.smShowErrorAlertController(message: message)
                    }
                }
            }),
            UIAlertAction(title: "Cancel", style: .default, handler: nil)
        ])
    }


}

