//
//  LabelTableVC.swift
//  Hulas
//
//  Created by Nhuja on 11/10/20.
//

import UIKit

class LabelTableVC: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    
    weak var delegate: ProvinceClickedProtocol?
    
    var labelList: [String]!
    var previousLabel: String!
    
    var isProvince:Bool = false
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    static func myInit(labelList: [String], previousValue: String) -> LabelTableVC {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let labelTableVC  = storyboard.instantiateViewController(withIdentifier: "LabelTableVC") as! LabelTableVC
        labelTableVC.labelList = labelList
        labelTableVC.previousLabel = previousValue
        return labelTableVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if isProvince {
            self.navigationItem.title = "Select Province"
        } else {
            self.navigationItem.title = "Select District"
        }
        navigationController?.navigationBar.tintColor = .white
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(addTapped))
        tblView.dataSource = self
        tblView.delegate = self
        tblView.register(LabelCell.nib(), forCellReuseIdentifier: LabelCell.identifier)
    }
    
    @objc func addTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}

extension LabelTableVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labelList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: LabelCell.identifier) as! LabelCell
        cell.configLabel(text: labelList[indexPath.row])
        if labelList[indexPath.row] == previousLabel {
            cell.accessoryType = .checkmark
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isProvince {
            self.delegate?.getNewProvince(newProvince: labelList[indexPath.row])
        } else {
            self.delegate?.getNewDistrict(newDistrict: labelList[indexPath.row])
        }
        self.dismiss(animated: true, completion: nil)
        
        
    }
    
}

extension LabelTableVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
}
