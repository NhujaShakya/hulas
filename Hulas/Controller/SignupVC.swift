//
//  SignupVC.swift
//  Hulas
//
//  Created by Nhuja on 11/10/20.
//

import UIKit
import MapKit
import SwiftSpinner

class SignupVC: UIViewController {
    
    @IBOutlet weak var photoImageView: CircleImageView!
    
    @IBOutlet weak var firstNameTxtField: UITextField!
    @IBOutlet weak var lastNameTxtField: UITextField!
    
    @IBOutlet weak var mobileTxtfield: UITextField!
    @IBOutlet weak var provinceBtn: BorderButton!
    @IBOutlet weak var districtBtn: BorderButton!
    @IBOutlet weak var metroTxtField: UITextField!
    @IBOutlet weak var wardTxtField: UITextField!
    @IBOutlet weak var streetTxtField: UITextField!
    @IBOutlet weak var usernameTxtFIeld: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var confirmPasswordTxtField: UITextField!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var vanpanTxtField: UITextField!
    @IBOutlet weak var vatpanSwitch: CurveSwitch!
    @IBOutlet weak var iAgreeBtn: UIButton!
    
    @IBOutlet weak var imageBtn: UIButton!
    @IBOutlet weak var myScrollView: UIScrollView!
    
    private var provinceApi = ProvinceApi()
    private var locationManager = CLLocationManager()
    private var latitude: String = ""
    private var longitude: String = ""
    private var imageStr64: String = ""
    
    
    var selectedProvince: Province = Province()
    var selectedDistrict: District = District()
    
    var provinces: [Province] = []
    var signApi = SignUpApi()
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.tintColor = AppColor
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        findLocation()
        getProvinces()
        
        initNavigationSetup()
        initSetup()
        check()
    }
    
    func check() {
        firstNameTxtField.text = "Nhuja"
        lastNameTxtField.text = "Shakya"
        mobileTxtfield.text = "9860684170"
        metroTxtField.text = "jdbchjvbehjv"
        wardTxtField.text = "6"
        streetTxtField.text = "hbebfhvbe"
        usernameTxtFIeld.text = "nhujas"
        passwordTxtField.text = "123890123890"
        confirmPasswordTxtField.text = "123890123890"
        emailTxtField.text = "nhujas@gmail.com"
        vanpanTxtField.text = "1234567890"
    }
    
    func initSetup() {
        
        photoImageView.isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapped(_:)))
        tap.numberOfTapsRequired = 1
        photoImageView.addGestureRecognizer(tap)
        
        myScrollView.keyboardDismissMode = .onDrag
    }
    
    func initNavigationSetup() {
        self.navigationController?.navigationBar.titleTextAttributes =
        [NSAttributedString.Key.foregroundColor: AppColor,
         NSAttributedString.Key.font: UIFont(name: "poppins-medium", size: 20)!]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
    
    @IBAction func provinceBtnClicked(_ sender: Any) {
        guard !provinces.isEmpty else {
            getProvinces()
            showToast(message: "Loading...")
            return
        }
        let naviController = UINavigationController()
        naviController.navigationBar.barStyle = .black
        
        var _provinces = [String]()
        for _province in provinces {
            _provinces.append(_province.name)
        }
        let vc = LabelTableVC.myInit(labelList: _provinces, previousValue: selectedProvince.name)
//        vc.modalPresentationStyle = .currentContextr
        vc.delegate  = self
        vc.isProvince = true
        naviController.pushViewController(vc, animated: true)
        self.present(naviController, animated: true, completion: nil)
    }
    
    @IBAction func vatpanClicked(_ sender: Any) {
        if vatpanSwitch.isOn {
            vanpanTxtField.placeholder = "Enter the PAN Number.."
        } else {
            vanpanTxtField.placeholder = "Enter the VAT Number.."
        }
        
    }
    
    @IBAction func iAgreeBtnClicked(_ sender: Any) {
        self.iAgreeBtn.isSelected = !iAgreeBtn.isSelected
    }
    
    @IBAction func termAndConditionBtnClicked(_ sender: Any) {
        UIApplication.shared.open(URL(string : TERMSANDCONDITIONS_URL)!, options: [:], completionHandler: { (status) in
            })

    }
    
    
    
    @IBAction func signupBtnClicked(_ sender: Any) {
        
        guard let image = photoImageView.image, !image.isEqualsTo(UIImage(systemName: "camera.fill")) else {
            smShowErrorAlertController(message: "Please select Image.")
            return
        }
        guard let firstName = firstNameTxtField.text, !firstName.isEmpty else {
            smShowErrorAlertController(message: "First name is empty")
            return }
        guard let lastName = lastNameTxtField.text, !lastName.isEmpty else {
            smShowErrorAlertController(message: "Last name is empty")
            return }
        guard let mobile = mobileTxtfield.text, !mobile.isEmpty, mobile.count == 10 else {
            smShowErrorAlertController(message: "Mobile is empty")
            return
        }
        guard selectedProvince.id != 0 else {
            smShowErrorAlertController(message: "Province is empty")
            return
        }
        guard let ward = wardTxtField.text, !ward.isEmpty else {
            smShowErrorAlertController(message: "Ward no. is empty")
            return }
        guard let street = streetTxtField.text, !street.isEmpty else {
            smShowErrorAlertController(message: "Street is empty")
            return }
        guard let username = usernameTxtFIeld.text, !username.isEmpty else {
            smShowErrorAlertController(message: "Username is empty")
            return }
        guard let password = passwordTxtField.text, !password.isEmpty else {
            smShowErrorAlertController(message: "Password is empty")
            return
        }
        guard password == confirmPasswordTxtField.text else {
            
            smShowErrorAlertController(message: "Password and Confirm password doesnot match.")
            return
        }
        guard let panVat = vanpanTxtField.text , !panVat.isEmpty else {
            if vatpanSwitch.isOn {
                smShowErrorAlertController(message: "PAN is Empty")
            } else {
                smShowErrorAlertController(message: "VAT is Empty")
            }
            return
        }
        
        guard iAgreeBtn.isSelected == true else{
            smShowErrorAlertController(message: "You must agree to the terms")
            return }
        guard !latitude.isEmpty, !longitude.isEmpty else {
            smShowErrorAlertController(message: "Location is needed. Please Enable in the settings app.")
            return}
        
        SwiftSpinner.show("Registering...")
        
        
        
        let user = User()
        user.firstName = firstName
        user.lastName = lastName
        user.image = imageStr64
        user.mobile = mobile
        user.province = selectedProvince
        user.district = selectedDistrict
        user.metropolitan = metroTxtField.text ?? ""
        user.wardNo = ward
        user.street = street
        user.email = emailTxtField.text ?? ""
        user.userName = username
        user.password = password
        user.vatPan = panVat
        user.vatPanBool = !vatpanSwitch.isOn
        user.latitude = latitude
        user.longitude = longitude
        DispatchQueue.main.async {
            self.signApi.getSignUp(user: user) { (status, message) in
                SwiftSpinner.hide()
                if status {
                    let naviController = UINavigationController()
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
                    naviController.pushViewController(vc, animated: true)
                    naviController.modalPresentationStyle = .fullScreen
                    self.present(naviController, animated: true, completion: nil)
                } else {
                    self.smShowErrorAlertController(message: message)
                }
            }
        }
    }
    
    private func getProvinces() {
        provinceApi.getProvince { (status, message, provinces) in
            if status {
                self.provinces = provinces
                self.selectedProvince = provinces[0]
                self.provinceBtn.setTitle(self.selectedProvince.name, for: .normal)
                self.provinceBtn.setTitleColor(.black, for: .normal)
//                self.selectedDistrict = self.selectedProvince.districts[0]
//                self.districtBtn.setTitle(self.selectedDistrict.name, for: .normal)
//                self.districtBtn.setTitleColor(.black, for: .normal)
            } else {
                self.smShowErrorAlertController(message: message)
            }
        }
    }

    @IBAction func districtBtnClicked(_ sender: Any) {
        guard !selectedProvince.districts.isEmpty else {
            getProvinces()
            showToast(message: "Province First")
            return
        }
        
        let naviController = UINavigationController()
        naviController.navigationBar.barStyle = .black
        
        var allDistrict = [String]()
        for district in selectedProvince.districts {
            allDistrict.append(district.name)
        }
        let vc = LabelTableVC.myInit(labelList: allDistrict, previousValue: selectedDistrict.name)
        vc.delegate  = self
        naviController.pushViewController(vc, animated: true)
        self.present(naviController, animated: true, completion: nil)
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
}

extension SignupVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    
    func launchImgPicker(source: UIImagePickerController.SourceType) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = source
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else { return }
        photoImageView.contentMode = .scaleAspectFill
        let userImage = photoImageView.image
        let imageData:NSData = userImage!.pngData()! as NSData
        imageStr64 = imageData.base64EncodedString(options: .lineLength64Characters)
        photoImageView.image = UIImage(data: image.jpegData(compressionQuality: 0.1)!)
        imageBtn.setTitle("Edit", for: .normal)
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func imageEditBtnClicked(_ sender: Any) {
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose your source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.launchImgPicker(source: .camera)
            } else {
                self.simpleAlert(title: "Error", message: "Camera not available")
            }
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            self.launchImgPicker(source: .photoLibrary)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        self.present(actionSheet, animated: true)
    }
    
    @objc func tapped(_ tap: UITapGestureRecognizer) {
        
        imageEditBtnClicked(self)
    }
    
}

extension SignupVC: ProvinceClickedProtocol {
    func getNewDistrict(newDistrict: String) {
        districtBtn.setTitleColor(.black, for: .normal)
        districtBtn.setTitle(newDistrict, for: .normal)
        selectedDistrict = selectedProvince.districts.filter({ (temp) -> Bool in
            let number:String = temp.name
            return number.contains(newDistrict)
        }).first!
    }
    
    func getNewProvince(newProvince: String) {
        provinceBtn.setTitle(newProvince, for: .normal)
        provinceBtn.setTitleColor(.black, for: .normal)
        selectedProvince = provinces.filter({ (temp) -> Bool in
            let number:String = temp.name
            return number.contains(newProvince)
        }).first!
        
//        self.selectedDistrict = selectedProvince.districts[0]
//        self.districtBtn.setTitle(self.selectedDistrict.name, for: .normal)
//        self.districtBtn.setTitleColor(.black, for: .normal)
    }
}

extension SignupVC: CLLocationManagerDelegate {
    
    func findLocation() {
         if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            self.locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
             // continuous request
//            locationManager.startUpdatingLocation()

             // one time request
            locationManager.requestLocation()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let _coordinate = manager.location?.coordinate {
            self.locationManager.stopUpdatingLocation()
            self.locationManager.delegate = nil

            latitude = String(_coordinate.latitude)
            longitude = String(_coordinate.longitude)
            print(latitude,",",longitude)
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
