//
//  LoginVC.swift
//  Hulas
//
//  Created by Nhuja on 11/10/20.
//

import UIKit
import SwiftSpinner

class LoginVC: UIViewController {
    
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var hideBtn: UIButton!
    @IBOutlet weak var passwordTxtField: UITextField!
    
    let loginApi = LoginApi()
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func forgotPasswordBtnClicked(_ sender: Any) {
        self.smShowErrorAlertController(message: "Contact the admins of Cyclone Nepal to change the password.")
    }
    
    @IBAction func loginBtnClicked(_ sender: Any) {
        self.view.endEditing(true)
        
        guard let email = emailTxtField.text, !email.isEmpty else {
            smShowErrorAlertController(message: "Email is empty")
            return }
        guard let password = passwordTxtField.text, !password.isEmpty else {
            smShowErrorAlertController(message: "Password is empty")
            return
        }
        SwiftSpinner.show("Logging In... ")
        
        loginApi.getLogin(panVatMobile: email, password: password) { (status, message) in
            SwiftSpinner.hide()
            if status {
                let naviController = UINavigationController()
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
                naviController.pushViewController(vc, animated: true)
                naviController.modalPresentationStyle = .fullScreen
                self.present(naviController, animated: true, completion: nil)
            } else {
                self.smShowErrorAlertController(message: message)
            }
        }
        
        
        
    }
    
    @IBAction func signupBtnClicked(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func hiddePasswordBtnClicked(_ sender: Any) {
        hideBtn.isSelected = !hideBtn.isSelected
        passwordTxtField.isSecureTextEntry = !hideBtn.isSelected
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    

}
